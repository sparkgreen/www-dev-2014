<?php

require("pdf/fpdf.php");
require("pdf/fpdi.php");
require("pdf/sapdf.php");

foreach($_POST as $key => $val){
	$_NUMBERS[$key] = str_replace(",","",$val);
}

function freqtext($int){
	switch($int){
		case 1:
			return "year";
			break;
		case 12:
			return "month";
			break;
		case 26:
			return "fortnight";
			break;
		case 52:
			return "week";
			break;
		default:
			return freqtext(1);
	}
}

function generateDate($pdf){
	$pdf->SetFont('Helvetica','','9');
	$pdf->SetTextColor(21,70,121);
	$pdf->SetXY(159, 766);
	$pdf->Cell(287, 9, date('d/m/Y'), 0, 0, 'C');
}
function hex2dec($couleur = "#000000"){
    $R = substr($couleur, 1, 2);
    $rouge = hexdec($R);
    $V = substr($couleur, 3, 2);
    $vert = hexdec($V);
    $B = substr($couleur, 5, 2);
    $bleu = hexdec($B);
    $tbl_couleur = array();
    $tbl_couleur['R']=$rouge;
    $tbl_couleur['V']=$vert;
    $tbl_couleur['B']=$bleu;
    return $tbl_couleur;
}

//conversion pixel -> millimeter at 72 dpi
function px2mm($px){
    return $px*25.4/72;
}

function txtentities($html){
    $trans = get_html_translation_table(HTML_ENTITIES);
    $trans = array_flip($trans);
    return strtr($html, $trans);
}


$importChart = true;

if($importChart){

	$fields = array(
		'filename'=>urlencode('chart'),
		'type'=>urlencode('image/png'),
		'width'=>urlencode('430'),
		'svg'=>urlencode($_POST['sgSvgContent'])
	);
	$fields_string = "";
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string,'&');
	$rand  = rand(1000,9999);
	$temp = sys_get_temp_dir();
	$file = $temp.'/svg'.$rand.'.png';

	$fp = fopen($file , 'w');
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch,CURLOPT_URL,'http://export.highcharts.com/');
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1); // don't use a cached version of the url 
	curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	fclose($fp);
	
} else {
	$file = sys_get_temp_dir().'svg8515.png';
}

$pdf = new SAPDF("P","pt","A4");

$pdf->SetDisplayMode('fullpage');


//$pdf->AddFont('HelveticaNeueLT-BoldOutline','',"HelveticaLT75BoldOutline.php");
$pdf->AddFont('Helvetica-Bold','',"helveticab.php");
$pdf->AddFont('HelveticaNeueLT-Light','',"HelveticaLT45Light.php");

$rollover = ($_POST['sgFundType'] == "Flexible Rollover") ? true : false;
$spouse = ($_POST['sgIncludeSpouse'] == "N") ? false : true;

if($rollover){
	$sslink = "http://www.supersa.sa.gov.au/knowledge_centre/financial_planning";
	$cclink = "http://www.supersa.sa.gov.au/calculator_resources/after_tax_contributions__flexible_rollover_product";
	$invlink = "http://www.supersa.sa.gov.au/files/import960-255_frpfm05.pdf?nc=922";
} elseif($_POST['sgFundType'] == "Triple S"){
	$sslink = "http://www.supersa.sa.gov.au/calculator_resources/salary_sacrifice__triple_s";
	$cclink = "http://www.supersa.sa.gov.au/calculator_resources/after_tax_contributions__triple_s";
	$invlink = "http://www.supersa.sa.gov.au/files/import960-260_tsfm11.pdf?nc=585";
} else {
	$sslink = "http://www.supersa.sa.gov.au/calculator_resources/salary_sacrifice__super_sa_select";
	$cclink = "http://www.supersa.sa.gov.au/calculator_resources/after_tax_contributions__super_sa_select";
	$invlink = "http://www.supersa.sa.gov.au/files/1183_investment_choice_select_form.pdf?nc=634";
}

if($spouse){
	$pdf->setSourceFile("assets/report-with-spouse-production.pdf");
} else {
	$pdf->setSourceFile("assets/report-no-spouse-production.pdf");
}

/////////////
//	PAGE 1 //
/////////////

$page = $pdf->importPage(1,'/MediaBox');
$pdf->addPage();
$pdf->useTemplate($page);

$pdf->SetFont('Helvetica-Bold','','14');
$pdf->SetTextColor(21,70,121);

// set age
$pdf->Text(104,166,$_POST['sgAge']);//154

// set sex
$text = ($_POST['sgSex'] == "F") ? "Female" : "Male";
$pdf->Text(125,196,$text);//184

// set income
$text = ($rollover) ? "N/A" : $_POST['sgIncome'];
$pdf->Text(185,228,$text);//216

// set balance
$pdf->Text(274,259,$_POST['sgBalance']);//247

// set BT
$bt = ($_POST['sgContBTPerc'] * 100) + (($_POST['sgBefore'] == "Y") ? $_POST['sgBeforePerc'] : 0);
$btd = $_NUMBERS['sgContBTDollar'] + (($_POST['sgBefore'] == "Y") ? $_NUMBERS['sgBeforeDollar'] * $_POST['sgBeforeFreq'] : 0);
$btdf = $btd / $_POST['accordion-primary-cont-freq'];
$text = ($bt == 0 && $btd == 0) ? "No" : "Yes";
$bt = ($rollover) ? "N/A" : $bt;
$btd = ($rollover) ? "N/A" : $btd;

$pdf->Text(63,290,$text);//373
$pdf->Text(274,290,$bt);//278
$pdf->Text(380,290,number_format($btdf,0,".",","));//596
$pdf->Text(460,290,freqtext($_POST['accordion-primary-cont-freq']));//596

// set AT
$at = ($_POST['sgContATPerc'] * 100) + (($_POST['sgAfter'] == "Y") ? $_POST['sgAfterPerc'] : 0);
$at = ($rollover) ? "N/A" : $at;
$text = ($at == 0) ? "No" : "Yes";
$pdf->Text(63,321,$text);//373
$pdf->Text(274,321,$at);//309

// set TARGET
$text = ($_POST['tb'] == "N") ? "No" : "Yes";
$pdf->Text(63,354,$text);//373
$text = ($_POST['tb'] == "N") ? "N/A" : $_POST['sgPlanBalance'];
$pdf->Text(274,354,$text);//342

// set SPOUSE INCLUSION
$text = ($spouse) ? "Yes" : "No";
$pdf->Text(63,385,$text);//373

if($spouse){
	// set spouse age
	$pdf->Text(104,484,$_POST['sgAgeSpouse']);//472

	// set spouse sex
	$text = ($_POST['sgSexSpouse'] == "F") ? "Female" : "Male";
	$pdf->Text(125,515,$text);//503

	// set spouse income
	$text = ($rollover) ? "N/A" : $_POST['sgIncomeSpouse'];
	$pdf->Text(185,546,$text);//534

	// set spouse balance
	$pdf->Text(274,577,$_POST['sgBalanceSpouse']);//565

	// set spouse BT
	$bts = ($_POST['sgContBTPercSpouse'] * 100) + (($_POST['sgBeforeSpouse'] == "Y") ? $_POST['sgBeforePercSpouse'] : 0);
	$btds = $_NUMBERS['sgContBTDollarSpouse'] + (($_POST['sgBeforeSpouse'] == "Y") ? $_NUMBERS['sgBeforeDollarSpouse'] * $_POST['sgBeforeFreqSpouse'] : 0);
	$btdfs = $btds / $_POST['accordion-spouse-cont-freq'];
	$text = ($bts == 0 && $btds == 0) ? "No" : "Yes";
	
	$bts = ($rollover) ? "N/A" : $bts;
	$btds = ($rollover) ? "N/A" : number_format($btds,0,".",",");
	
	$pdf->Text(63,608,$text);//373
	$pdf->Text(274,608,$bts);//596
	$pdf->Text(380,608,number_format($btdfs,0,".",","));//596
	$pdf->Text(460,608,freqtext($_POST['accordion-spouse-cont-freq']));//596

	// set spouse AT
	$ats = ($_POST['sgContATPercSpouse'] * 100) + (($_POST['sgAfterSpouse'] == "Y") ? $_POST['sgAfterPercSpouse'] : 0);
	$text = ($ats == 0) ? "No" : "Yes";
	$ats = ($rollover) ? "N/A" : $ats;
	$pdf->Text(63,640,$text);//373
	$pdf->Text(274,640,$ats);//628

}

generateDate($pdf);

/////////////
//	PAGE 2 //
/////////////

$page = $pdf->importPage(2,'/MediaBox');
$pdf->addPage();
$pdf->useTemplate($page);



$pdf->SetFont('HelveticaNeueLT-Light','','14');
$pdf->SetTextColor(255,255,255);

// set retirement balance
$pdf->SetXY(36, 182);
$pdf->Cell(96, 24, $_POST['sgRetireBalance'], 0, 0, 'C');

// set retirement income
$text = '$'.number_format($_POST['sgRetireIncome'],0,'.',',');
$pdf->SetXY(142, 182);
$pdf->Cell(96, 24, $text, 0, 0, 'C');

// set super runs out
$pdf->SetFont('HelveticaNeueLT-Light','','40');
$pdf->SetXY(249, 173);
$pdf->Cell(96, 46, $_POST['sgSuperRunsOut'], 0, 0, 'C');

// set life expt
$pdf->SetXY(356, 173);
$pdf->Cell(96, 46, $_POST['sgLifeExpt'], 0, 0, 'C');



if($_POST['sgGap'] < 0){
	$gaptext = "GAP";
	// ORANGE
	$pdf->SetDrawColor(249,185,0);
	$pdf->SetFillColor(249,185,0);
} else {
	$gaptext = "SUPER BUFFER";
	// GREEN
	$pdf->SetDrawColor(201,221,59);
	$pdf->SetFillColor(201,221,59);
}
// place circle
$pdf->Circle(509, 197, 48,'F'); //463,149 + radius
// set gap
$pdf->SetXY(463, 173);
$pdf->Cell(96, 46, $_POST['sgGap'], 0, 0, 'C');
// set gap text
$pdf->SetFont('HelveticaNeueLT-Light','','12');
$pdf->SetTextColor(125,132,136);
$pdf->SetXY(463, 254);
$pdf->Cell(96, 36, $gaptext, 0, 0, 'C');

// import chart

if($importChart){

	if($httpCode != 200){
		$pdf->SetFont('Helvetica','','18');
		$pdf->SetTextColor(84,88,166);
		$pdf->Text(342,120,"Chart unavailable");
		$pdf->Text(362,120,"Please try again later");
	} else {
		$pdf->Image($file,111,342,440,200);
	}
	unlink($file);
	
} else {

	$pdf->Image($file,111,342,440,200);
}

// set life exp label
$pdf->SetFont('HelveticaNeueLT-Light','','10');
$pdf->SetTextColor(125,132,136);
$lifeX = (($_POST['sgLifeExpt'] - $_POST['sgAge']) / (110 - $_POST['sgAge'])*440) + 30;
$superX = (($_POST['sgSuperRunsOut'] - $_POST['sgAge']) / (110 - $_POST['sgAge'])*440) + 15;
if($_POST['sgLifeExpt'] < $_POST['sgSuperRunsOut']){
	$pdf->Text($lifeX,337,"Life expectancy ".$_POST['sgLifeExpt']);
	$pdf->Text($superX,325,"Super runs out at ".$_POST['sgSuperRunsOut']);
} else {
	$pdf->Text($lifeX,325,"Life expectancy ".$_POST['sgLifeExpt']);
	$pdf->Text($superX,337,"Super runs out at ".$_POST['sgSuperRunsOut']);
}

// set Y labels
$pdf->SetFont('HelveticaNeueLT-Light','','14');
$pdf->SetTextColor(21,70,121);

$pdf->SetXY(22, 342);
$pdf->Cell(88, 14, '$'.  number_format($_POST['sgMaxChartHeight'],0,'.',','), 0, 0, 'R');

$pdf->SetXY(22, 389);
$pdf->Cell(88, 14, '$'.  number_format($_POST['sgMaxChartHeight']*.75,0,'.',','), 0, 0, 'R');

$pdf->SetXY(22, 436);
$pdf->Cell(88, 14, '$'.  number_format($_POST['sgMaxChartHeight']*.5,0,'.',','), 0, 0, 'R');

$pdf->SetXY(22, 483);
$pdf->Cell(88, 14, '$'.  number_format($_POST['sgMaxChartHeight']*.25,0,'.',','), 0, 0, 'R');

$pdf->SetXY(22, 530);
$pdf->Cell(88, 14, '$'.  number_format($_POST['sgMaxChartHeight']*0,0,'.',','), 0, 0, 'R');

// set X labels

$minAgeLabel = ceil($_POST['sgAge']/10)*10;
$cols = (110 - $minAgeLabel) / 10;
$width = 440 / $cols;
for($i=0;$i<$cols;$i++){
	$pdf->Text(111 + $i*$width,556,$minAgeLabel + $i*10);
}




if($spouse){
	
	$pdf->SetFont('Helvetica-Bold','','14');
	$pdf->SetTextColor(21,70,121);
	$pdf->Text(310,587,$_POST['sgRetireAge']);//575

	// set spouse retireage
	$pdf->Text(310,616,$_POST['sgRetireAgeSpouse']);//604
} else {
	$pdf->SetFont('Helvetica-Bold','','14');
	$pdf->SetTextColor(21,70,121);
	$pdf->Text(310,606,$_POST['sgRetireAge']);//575
}

// set income
$text = '$'.number_format($_POST['sgRetireIncome'],0,'.',',');
$pdf->Text(323,644,$text);//632



if($spouse){
	
	// set hours perc
	$text = ($_POST['accordion-primary-working'] == 'Y') ? $_POST['accordion-primary-working-perc'] : '-';
	$pdf->Text(323,682,$text);//670

	// set hours age
	$text = ($_POST['accordion-primary-working'] == 'Y') ? $_POST['accordion-primary-working-age'] : '-';
	$pdf->Text(397,682,$text);//670

	// set hours from
	$text = ($_POST['accordion-primary-career'] == 'Y') ? $_POST['accordion-primary-career-from'] : '-';
	$pdf->Text(341,710,$text);//398

	// set hours to
	$text = ($_POST['accordion-primary-career'] == 'Y') ? $_POST['accordion-primary-career-to'] : '-';
	$pdf->Text(395,710,$text);//398
	
	// set hours perc
	$text = ($_POST['accordion-spouse-working'] == 'Y') ? $_POST['accordion-spouse-working-perc'] : '-';
	$pdf->Text(323,748,$text);//736

	// set hours age
	$text = ($_POST['accordion-spouse-working'] == 'Y') ? $_POST['accordion-spouse-working-age'] : '-';
	$pdf->Text(397,748,$text);//736

	// set hours from
	$text = ($_POST['accordion-spouse-career'] == 'Y') ? $_POST['accordion-spouse-career-from'] : '-';
	$pdf->Text(341,776,$text);//764

	// set hours to
	$text = ($_POST['accordion-spouse-career'] == 'Y') ? $_POST['accordion-spouse-career-to'] : '-';
	$pdf->Text(395,776,$text);//764
} else {
	
	// set hours perc
	$text = ($_POST['accordion-primary-working'] == 'Y') ? $_POST['accordion-primary-working-perc'] : '-';
	$pdf->Text(323,692,$text);//670

	// set hours age
	$text = ($_POST['accordion-primary-working'] == 'Y') ? $_POST['accordion-primary-working-age'] : '-';
	$pdf->Text(397,692,$text);//670

	// set hours from
	$text = ($_POST['accordion-primary-career'] == 'Y') ? $_POST['accordion-primary-career-from'] : '-';
	$pdf->Text(341,722,$text);//398

	// set hours to
	$text = ($_POST['accordion-primary-career'] == 'Y') ? $_POST['accordion-primary-career-to'] : '-';
	$pdf->Text(395,722,$text);//398
}

//generateDate($pdf);

/////////////
//	PAGE 3 //
/////////////

$page = $pdf->importPage(3,'/MediaBox');
$pdf->addPage();
$pdf->useTemplate($page);

if($_POST['sgContATPerc']  + $_POST['sgContBTPerc'] + $_POST['sgContBTDollar'] > 0){
	$dollars = number_format(round($btdf + ($bt * $_NUMBERS['sgIncome'] / $_POST['accordion-primary-cont-freq'] / 100)),0,".",",");
	$a0[] = "BT";
	$a1[] = $bt . '%';
	$a1a[] = ($btdf > 0) ? '+$'.number_format($btdf,0,".",",") : "";
	$a1b[] = ' PER '.strtoupper(freqtext($_POST['accordion-spouse-cont-freq']));
	$a2[] = 'BEFORE TAX CONTRIBUTIONS';
	$a3[] = 'Your employer deducts $'.$dollars.' per '.freqtext($_POST['accordion-primary-cont-freq']).' before tax';
	$a4[] = 'Salary Sacrifice';
	$a5[] = 'Set up contributions using the employer form';
	$a6[] = 'KNOWLEDGE';

	$dollars = number_format(round($at * str_replace(',','',$_POST['sgIncome']) / $_POST['accordion-primary-cont-freq'] / 100),0,".",",");
	$a0[] = "AT";
	$a1[] = $at . '%';
	$a1a[] = "";
	$a1b[] = "";
	$a2[] = 'AFTER TAX CONTRIBUTIONS';
	$a3[] = 'You contribute $'.$dollars.' per '.freqtext($_POST['accordion-primary-cont-freq']).' after tax';
	$a4[] = 'Co'."\r\n".'Contrbution';
	$a5[] = 'Contribution rate change form';
	$a6[] = 'KNOWLEDGE';
}

if($spouse){
	if($_POST['sgContATPercSpouse'] + $_POST['sgContBTPercSpouse'] + $_POST['sgContBTDollarSpouse'] > 0){
		$dollars = number_format(round($btdfs + ($bts * $_NUMBERS['sgIncomeSpouse'] / $_POST['accordion-spouse-cont-freq'] / 100)),0,".",",");
		$a0[] = "BTS";
		$a1[] = $bts . '%';
		$a1a[] = ($btdfs > 0) ? '+$'.number_format($btdfs,0,".",",") : "";
		$a1b[] = ' PER '.strtoupper(freqtext($_POST['accordion-spouse-cont-freq']));
		$a2[] = 'SPOUSE EXTRA BEFORE TAX';
		$a3[] = 'Their employer deducts $'.$dollars.' per '.freqtext($_POST['accordion-spouse-cont-freq']).' before tax';
		$a4[] = 'Salary Sacrifice';
		$a5[] = 'Go to the Knowledge Centre to get started';
		$a6[] = 'FUND';

		$dollars = number_format(round($ats * str_replace(',','',$_POST['sgIncomeSpouse']) / $_POST['accordion-spouse-cont-freq'] / 100),0,".",",");
		$a0[] = "ATS";
		$a1[] = $ats . '%';
		$a1a[] = "";
		$a1b[] = "";
		$a2[] = 'SPOUSE EXTRA AFTER TAX';
		$a3[] = 'They contribute $'.$dollars.' per '.freqtext($_POST['accordion-spouse-cont-freq']).' after tax';
		$a4[] = 'Co'."\r\n".'Contrbution';
		$a5[] = 'Go to the Knowledge Centre to get started';
		$a6[] = 'FUND';
	}
}

$a0[] = "INV";
$a1[] = strtoupper($_POST['sgInvNameAcc']);
$a1a[] = "";
$a1b[] = "";
$a2[] = 'INVESTMENT CHOICE';
$a3[] = 'Change your investment type';
$a4[] = 'Investment';
$a5[] = 'Investment choice form';
$a6[] = 'KNOWLEDGE';

for($i=0;$i<count($a1);$i++){
	$j = $i*104; 

	$pdf->Image('assets/actions-column.png',36+$j,144,104,279);
	$pdf->SetTextColor(255,255,255);
	if($a0[$i]=="INV"){
		
		$pdf->SetFont('HelveticaNeueLT-Light','','14');
		$pdf->SetXY(45+$j, 170);
		$pdf->Cell(86, 31, $a1[$i], 0, 0, 'C');
	} elseif(strlen($a1a[$i]) > 0){
		
		$pdf->SetFont('HelveticaNeueLT-Light','','18');
		$pdf->SetXY(45+$j, 160);
		$pdf->Cell(86, 31, $a1[$i], 0, 0, 'C');
		if(strlen($a1a[$i]) > 0){
			$pdf->SetFont('HelveticaNeueLT-Light','','9');
			$pdf->SetXY(45+$j, 175);
			$pdf->Cell(86, 31, $a1a[$i]." ".$a1b[$i], 0, 0, 'C');
			//$pdf->SetXY(45+$j, 180);
			//$pdf->Cell(86, 31, $a1b[$i], 0, 0, 'C');
		}
	} else {
		$pdf->SetFont('HelveticaNeueLT-Light','','30');
		$pdf->SetXY(45+$j, 170);
		$pdf->Cell(86, 31, $a1[$i], 0, 0, 'C');
	}

	$pdf->SetFont('HelveticaNeueLT-Light','','11');
	$pdf->SetTextColor(21,70,121);
	$pdf->SetXY(42+$j, 231);
	$pdf->MultiCell(92, 15, $a2[$i], 0, 'C');

	$pdf->SetFont('HelveticaNeueLT-Light','','10');
	$pdf->SetTextColor(125,132,136);
	$pdf->SetXY(42+$j, 269);
	$pdf->MultiCell(100, 11, $a3[$i], 0, 'L');

	$pdf->SetFont('HelveticaNeueLT-Light','','16');
	$pdf->SetTextColor(255,255,255);
	$pdf->SetXY(42+$j, 315);
	$pdf->MultiCell(90, 16, $a4[$i], 0, 'L');
	
	$pdf->SetFont('HelveticaNeueLT-Light','','10');
	$pdf->SetTextColor(255,255,255);
	$pdf->SetXY(42+$j, 348);
	$pdf->MultiCell(90, 10, $a5[$i], 0, 'L');
	
	if($a6[$i] == "KNOWLEDGE"){
		$pdf->SetFont('HelveticaNeueLT-Light','','10');
		$pdf->SetTextColor(125,132,136);
		$pdf->SetXY(42+$j, 386);
		$pdf->MultiCell(100,11,"Want to know more?");
		$pdf->SetXY(42+$j, 397);
		$pdf->MultiCell(100,11,"Go to the");
		$pdf->SetXY(42+$j, 408);
		$pdf->WriteHTML('<a target="_blank" href="http://www.supersa.sa.gov.au/knowledge_centre">Knowledge Centre</a>');

	} else {
		$pdf->SetFont('HelveticaNeueLT-Light','','10');
		$pdf->SetTextColor(125,132,136);
		$pdf->SetXY(42+$j, 386);
		$pdf->MultiCell(100, 11, "If your spouse is not with us, contact their fund directly for help", 0, 'L');
		
	}
	
	if($a0[$i]=="BT"){
		$pdf->Link(42+$j, 348, 90, 30, $sslink);
	} elseif($a0[$i]=="AT"){
		$pdf->Link(42+$j, 348, 90, 30, $cclink);
	} elseif($a0[$i]=="INV"){
		$pdf->Link(42+$j, 348, 90, 30, $invlink);
	}
	
}

$pdf->Link(36, 444, 149,112, 'http://www.supersa.sa.gov.au/knowledge_centre/financial_planning');
$pdf->Link(205, 444, 149,112, 'http://www.supersa.sa.gov.au/files/920_easy_roll_in_current.pdf?nc=585');

//generateDate($pdf);

/////////////
//	PAGE 4 //
/////////////

$page = $pdf->importPage(4,'/MediaBox');
$pdf->addPage();
$pdf->useTemplate($page);

// set inflation
$pdf->SetFont('Helvetica-Bold','','14');
$pdf->SetTextColor(21,70,121);
$pdf->Text(155,166,$_POST['sgInflationRate']);//154

// set salary inflation
$pdf->Text(155,197,$_POST['sgSalInc']);//185

// set insurance
$pdf->Text(454,166,number_format(round($_POST['sgInsurance65']/52,2),2));//154

// set ip insurance
$pdf->Text(454,197,$_POST['sgInsuranceIP65']);//185

// set age pension
$text = ($_POST['sgIncludeAgePension'] == 'Y') ? "Yes" : "No";
$pdf->Text(63,228,$text);//216

// set lump sum
$text = number_format($_POST['sgLumpSum']*1,0,".",",");
$pdf->Text(454,229,$text);//217

// set inv type
$pdf->Text(36,323,$_POST['sgInvNameAcc']);//311

// set inv type pension
$pdf->Text(36,355,$_POST['sgInvNamePen']);//343

// set fees
$pdf->Text(320,325,$_POST['sgFixedFee']);//313

// set generation date

$disclaimer = "This projector is designed to help you estimate what your superannuation benefit might be at your retirement and your possible income in retirement.

Any quotation or projection provided to you by this calculator in respect of your future superannuation benefit entitlement is an estimate only based on the information you have inserted into the calculator (including any default options), the current target investment return rate, the tax rules applicable to the product and the assumptions applicable to this calculator. None of the details you enter into this calculator are retained or saved by Super SA. The results generated by this calculator are not the responsibility of Super SA and should not be relied upon for the purpose of making a decision about your superannuation.  Once you become eligible for a superannuation entitlement, your final benefit entitlement will be calculated using the latest unit price available in accordance with the State legislation or Trust Deed and Rules governing the relevant product in respect of which your entitlement relates.

The superannuation products administered by Super SA are exempt public sector superannuation schemes and are not regulated by the Australian Securities and Investments Commission or the Australian Prudential Regulation Authority.  Super SA is not required to hold an Australian Financial Services licence to provide general advice about its superannuation products.

The information provided by this calculator is not financial advice and does not take into account your individual objectives, financial situation or needs. It also does not take into account fees and charges or differentiate between income types.  Super SA strongly recommends that before making any financial decisions, you consider the appropriateness of the information obtained from this calculator in the context of your own objectives, financial situation and needs, refer to the relevant PDS (including details of cooling off rights, if any) and seek financial advice from a licensed financial adviser in relation to your financial position and requirements.  No liability will be accepted by Super SA in relation to any claims, losses, damages, costs or expenses whatsoever which arise as a result of using this calculator.";

$pdf->SetFont('Helvetica','','9');
$pdf->SetTextColor(21,70,121);
$pdf->SetXY(34, 435);
$pdf->MultiCell(523, 12, $disclaimer, 0, 'L');

//generateDate($pdf);

return $pdf->Output('projection.pdf', 'I');



