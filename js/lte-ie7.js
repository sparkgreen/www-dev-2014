/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-checkbox-partial' : '&#xe001;',
			'icon-checkbox-unchecked' : '&#xe002;',
			'icon-checkbox-checked' : '&#xe003;',
			'icon-radio-unchecked' : '&#xe004;',
			'icon-radio-checked' : '&#xe005;',
			'icon-record' : '&#xe006;',
			'icon-caret-up' : '&#xf0d8;',
			'icon-caret-down' : '&#xf0d7;',
			'icon-untitled' : '&#xe007;',
			'icon-arrow-left' : '&#xe008;',
			'icon-arrow-down' : '&#xe00d;',
			'icon-arrow-up' : '&#xe00e;',
			'icon-check-empty' : '&#xe00f;',
			'icon-check-filled' : '&#xe010;',
			'icon-arrow-down-2' : '&#xe011;',
			'icon-arrow-up-2' : '&#xe012;',
			'icon-arrow' : '&#xe013;',
			'icon-menu' : '&#xe014;',
			'icon-close' : '&#xe015;',
			'icon-steps-triangle' : '&#xe016;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};